package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"sync"
	"time"

	"gitlab.com/codeforvenezuela/blackouts/powermonitor/pkg/publisher"
)

// The following script is intended to run as a daemon.
// It does the following:
// * On start, it checks the audit logs *detection-log-file* to determine if
//   there was an outage since last time it was running.
//   An outage is detected if the *detection-log-file* has not been touched in *touch-interval*.
// * When an outage is detected, a file is written to *detected-outages-path*.
//   This file contains a json with information about the outage.
// * A go routine is constantly checking for files in *detected-outages-path*.
//   When found, it emits to the Angostura and deletes them.
//   If there are failures emitting to the event but, files will be kept on disk
//   until process is able to emit them.

var (
	state                 = flag.String("state", "Miranda", "State that this power monitor is configured for.")
	municipality          = flag.String("municipality", "Libertador", "Municipality that this power monitor is configured for.")
	city                  = flag.String("city", "Caracas", "City that this power monitor is configured for.")
	powerMonitorId        = flag.String("power-monitor-id", "", "An UUID to identify this monitor")
	angosturaGateEndpoint = flag.String("angostura-gate-endpoint", "", "Endpoint to emit outages to angostura")

	detectionLogsFile        = flag.String("detection-log-file", "/tmp/pm_audit_logs", "File path that monitor will use to detect power outages.")
	detectedOutagesPath      = flag.String("detected-outages-path", "/tmp/outages", "File path that monitor will use to detect power outages.")
	outageDetectionThreshold = flag.Duration("outage-detection-threshold", 5*time.Minute, "Time between measures that will trigger an outage detection.")
	touchInterval            = flag.Duration("touch-interval", 1*time.Minute, "Time between measures to check if process is alive.")
)

func main() {
	flag.Parse()

	syslog, err := syslog.New(syslog.LOG_NOTICE, "power_monitor")
	if err != nil {
		log.Fatal(err)
	}

	// Now logs will go to syslog
	log.SetOutput(syslog)

	var mux sync.Mutex

	// First check if there was an outage before the last time this program
	detectAndPersistOutage(*detectionLogsFile, *outageDetectionThreshold)

	go powerUptimeUpdater(*detectionLogsFile, &mux, *touchInterval)
	go sendPowerOutagesToAngostura(*detectedOutagesPath, &mux)

	startedAt := time.Now()
	for {
		log.Printf("Power Monitor running since %v", startedAt)
		time.Sleep(1 * time.Hour)
	}
}

func touchAuditFile(file string) (err error) {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		emptyFile, err := os.Create(file)
		if err != nil {
			return err
		}
		emptyFile.Close()
		return err
	}

	now := time.Now()
	return os.Chtimes(file, now, now)
}

func lastMeasureTime(fileName string) (time.Time, error) {
	file, err := os.Stat(fileName)

	if err != nil {
		return time.Now(), err
	}

	return file.ModTime(), nil
}

func detectAndPersistOutage(fileName string, outageThreshold time.Duration) {
	lastMeasureTime, err := lastMeasureTime(fileName)
	if err != nil {
		log.Printf("Could not get measure. Do not report outage")
	}

	timeSinceLastMeasure := time.Now().Sub(lastMeasureTime)

	if timeSinceLastMeasure < outageThreshold {
		log.Printf("There is not outage since previous run. Time since last measure: %g minutes", timeSinceLastMeasure.Minutes())
		return
	}

	log.Printf("Outage detected. Power was out for: %g minutes", timeSinceLastMeasure.Minutes())
	powerOutageEven := publisher.PowerOutageEvent{
		StartTime:       lastMeasureTime.Unix(),
		DurationSeconds: timeSinceLastMeasure.Seconds(),
		State:           *state,
		Region:          *municipality,
		City:            *city,
		PowerMonitorId:  *powerMonitorId,
	}

	detectedOutageFile, err := ioutil.TempFile(*detectedOutagesPath, "outage-")
	if err != nil {
		log.Printf("Failed to create file to persist outage: %v", err)
		return
	}
	defer detectedOutageFile.Close()

	data, err := json.Marshal(&powerOutageEven)
	if err != nil {
		log.Printf("Failed to marshal power outage event to json file to persist outage: %v", err)
		return
	}
	_, err = detectedOutageFile.Write(data)

	if err != nil {
		log.Printf("Failed to marshal power outage event to json file to persist outage: %v", err)
		return
	}
	log.Printf("Successfully persisted power outage event. Details in %v", detectedOutageFile.Name())
}

func powerUptimeUpdater(fileName string, lock *sync.Mutex, touchInterval time.Duration) {
	for {
		func() {
			lock.Lock()
			defer lock.Unlock()

			err := touchAuditFile(fileName)

			if err != nil {
				log.Printf("Couldn't update power uptime log. Got error: %v", err)
				return
			}
			log.Printf("Successfully updated power uptime log: %v", fileName)

		}()

		// Now sleep for some time until updating the power uptime log
		log.Printf("Sleeping for %v before creating new logs", touchInterval)
		time.Sleep(touchInterval)
	}
}

func sendPowerOutagesToAngostura(outagesPath string, lock *sync.Mutex) {
	for {
		func() {
			lock.Lock()
			defer lock.Unlock()

			fileInfos, err := ioutil.ReadDir(*detectedOutagesPath)
			if err != nil {
				log.Printf("Couldn't read files from outages directory: %v", err)
				return
			}

			if len(fileInfos) == 0 {
				log.Printf("No outages pending to be sent to message bus.")
				return
			}

			for _, fileInfo := range fileInfos {
				fullPath := fmt.Sprintf("%s/%s", *detectedOutagesPath, fileInfo.Name())
				data, err := ioutil.ReadFile(fullPath)
				if err != nil {
					log.Printf("Couldn't open power outage event: %v", fullPath)
					return
				}
				powerOutageEvent := publisher.PowerOutageEvent{}
				err = json.Unmarshal(data, &powerOutageEvent)
				if err != nil {
					panic("Unexpected error unmarshalling power outage event")

				}
				publisher := publisher.NewProxyPublisher(*angosturaGateEndpoint)
				log.Printf("Emitting power outage: %v", powerOutageEvent)

				ctx := context.Background()

				err = publisher.Publish(ctx, &powerOutageEvent)
				if err != nil {
					log.Printf("Error emitting event to angostura: %v", err)
					continue
				}

				// Now clean up this from disk.
				// If program crashes here there could be dups.
				// YOLO.
				if err := os.Remove(fullPath); err != nil {
					log.Printf("Failed to delete file for power event outage: %v", fullPath)
					return
				}
			}

			log.Printf("Successfully emitted all pending events for power outages")
			return
		}()

		// Now sleep for some time until trying to emit
		log.Printf("Sleeping for %v seconds before checking for pending outages that haven't been sent to message bus.", 10*time.Second)
		time.Sleep(10 * time.Second)
	}
}
