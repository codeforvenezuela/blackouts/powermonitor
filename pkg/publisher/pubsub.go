package publisher

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	b64 "encoding/base64"
)

type PowerOutageEvent struct {
	StartTime       int64   `json:"start_time"`
	DurationSeconds float64 `json:"duration_seconds"`
	State           string  `json:"state"`
	City            string  `json:"city"`
	Region          string  `json:"region"`
	PowerMonitorId  string  `json:"power_monitor_id"`
}

type AngosturaPowerOutagePublisher interface {
	Publish(ctx context.Context, powerOutageEvent *PowerOutageEvent) error
}

type ProxyPowerOutagePublisher struct {
	endpoint string
}

func NewProxyPublisher(endpoint string) AngosturaPowerOutagePublisher {
	return &ProxyPowerOutagePublisher{endpoint: endpoint}
}

func (c *ProxyPowerOutagePublisher) Publish(ctx context.Context, powerOutageEvent *PowerOutageEvent) error {
	payload, err := json.Marshal(powerOutageEvent)
	if err != nil {
		return err
	}
	sEnc := b64.StdEncoding.EncodeToString(payload)
	eventStr := fmt.Sprintf("{'type':'power_outage_monitor', 'version': 1, 'payload': %s}", sEnc)
	event := []byte(eventStr)
	_, err = http.Post(c.endpoint, "application/json", bytes.NewBuffer(event))
	return err
}
